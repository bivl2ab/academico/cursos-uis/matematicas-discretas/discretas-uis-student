## Información del curso

Matemáticas Discretas 2021-2, Universidad Industrial de Santander.

* [Contenido, Objetivos, Metodología, Bibliografías y Requisitos UIS](https://www.uis.edu.co/webUIS/es/academia/facultades/fisicoMecanicas/escuelas/ingenieriaSistemas/programasAcademicos/ingenieriaSistemas/planEstudios.html)
* [2021-2. Calendario Académico vigente](https://www.uis.edu.co/webUIS/es/academia/calendariosAcademicos/2021/acuerdoAcad277_2021.pdf)


**Docente:** Juan A. Olmos

## Google Colaboratory

Durante el periodo académico utilizaremos la plataforma Google Colaboratory para el desarrollo de talleres, tareas y parciales. El único requisito es que usted cuente con un correo @gmail.com.

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb) 

- Para esto usted necesita una cuenta de gmail, luego entrar a google drive y donde desee crear un archivo de google Colaboratory.
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, usted puede escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en su navegador.

## Calificación
Los quices valen el doble que 1 tarea y la lista de ejercicios (o laboratorios) valen el triple que 1 tarea. 

**Observación:** 

- En caso de haber quices estos valen el doble que una tarea.
- En caso de haber talleres o listas de ejercicios estos valen el triple que una tarea.

|CORTES| 						  	 Porcentajes | Descripción|
|:---:			 	|:---					     |:---:|
|**PRIMER CORTE**| 						  	  | |
| 					| 10% Talleres y quices|Tarea circuito digital. Quiz 1 lógica. Tarea grupos cheveres y extraños. | 
| 					| 20% Parcial No. 1	 |Parcial de Lógica y Algoritmos| 
|**SEGUNDO CORTE**| 						  	  | |
| 					| 10% Talleres y quices|-| 
| 					| 20% Parcial No. 2	 |Parcial de Aritmética Modular y Conteo | 
|**TERCER CORTE**| 						  	  | |
| 					| 20% Parcial No. 3	 |Parcial de Relaciones y Grafos| 
|**PROYECTO FINAL**| 						 | |
| 					| 20% Proyecto Final|Pre-sustentacion del prototipo, Presentación, Sustentación, Repositorio.| 

## Calendario del semestre en curso (2021-2)
|Semana(Fecha)| | | Actividad|
|:---:			|:---						   |:---|---:  |
| |**LÓGICA** | | |
|``Sem_1``	|2 Nov-Presentación Curso | 5 Nov-Lógica (proposiciones, conectores y equivalencias lógicas)| - |
|``Sem_2``	|9 Nov-Lógica (Predicados y Cuantificadores) | 12 Nov-Demostraciones (Que es demostracion?. Algunos Enunciados conocidos. Deducciones Lógicas.)| 9 Nov (Tarea circuito lógico) --- 10 Nov (Asignada la lista ejercicios Logica) |
|``Sem_3``	|16 Nov-Demostraciones (Metodos de demostracion: directa, contraposicion, bicondicionales) | 19 Nov-Solo Quiz. Fallas internet en el aula| 19 de Nov (Quiz->Lógica) |
|``Sem_4``	|23 Nov-Demostraciones(Dem. por contradicción.) | 26 Nov-Inducción.  | - |
|	|**ALGORITMOS** | | |
|``Sem_5``	|30 Nov- Intro. Algoritmos | 3 Dic- Analisis de Complejidad de Alg.  | - |
|``Sem_6``	|7 Dic- Crecimiento de Funciones  | 10 Dic - Crecimiento de Funciones  | |
|``Sem_7``	|14 Dic- Examen| 17 Dic- Laboratorio| **--- PARCIAL No. 1 ---** (Entrega de ejercicios dia del Examen)|
|*receso academico*	| | | |
|	| **ARITMÉTICA MODULAR** | | |
|``Sem_8``	|11 Ene- Divisibilidad y Aritmetica modular | 14 Ene-Primos, Representacion de Enteros y Congruencias| Taller de Python |
| 	|**CONTEO** | | |
|``Sem_9``	|18 Ene- Aplicaciones Teoria de Numeros | 21 Ene- Principios basicos de Conteo, Inclusion y Exclusión| 18 de Ene --- Taller Teoría de Números |
|``Sem_10``	|25 Ene- Palomar, Permutaciones y Combinaciones | 28 Ene- Coef binomiales, Permutaciones y Combinaciones Gneralizadas| - |
|``Sem_11``	|1 Feb- Aplicaciones Combinatoria  | 4 Feb- Examen.| 1 de Feb [Laboratorio Combinatoria] 4 de Feb [**PARCIAL No. 2**] |
| 	|**RELACIONES** | | |
|``Sem_12``	|8 Feb- Relaciones y Propiedades| 11 Feb- Aplicaciones, Representaciones| 8 de Feb [**FASE 1. Tema**] |
|``Sem_13``	|15 Feb- Clausuras, Caminos y  algoritmos de relaciones | 18 Feb- Relaciones de Equivalencia, Orden y diagramas de Hasse| 18 de Feb [**FASE 2. Pre-Sustentación.***] |
| 	|**GRAFOS**| | |
|``Sem_14``	|22 Feb- Grafos y Modelos de Grafos, Terminología y modelos de grafos.| 25 Feb- Representación e Isomorfismos| - |
|``Sem_15``	|1 Mar- Caminos, Conectividad, Circuitos| 4 Mar- El camino mas corto, Grafos planares, Coloración| 4 de Mar [**FASE 3. Entrega de Revisión**]  |
|``Sem_16``	|8 Mar- Aplicaciones de Grafos | 11 Mar-Examen.  | 8 de Mar [**FASE 4. Sustentación***] 8 de Mar [Laboratorio Grafos] 11 de Mar [**PARCIAL No. 3**]|
|	|15 Mar- Entrega de Proyectos | 18 Mar -   | 14 de Mar [**FASE 5. Entrega Final**]|

# Proyecto Final:

* **Número de estudiantes.** Se aceptarán grupos de máximo 4 estudiantes. Realizar el proyecto de forma individual es también aceptado.
* **Funcionamiento del proyecto**: El proyecto se desarrollará en un repositorio (Github o Gitlab) . Dentro de él se debe realizar un notebook donde se desarrolle de forma explicativa el proyecto. El proyecto debe ser 100% funcional. ([tutorial]())
* **Uso de referencias.** Este es un proyecto realizado por estudiantes de Ingeniereia de Sistemas de la Universidad Industrial de Santander curso 2021-2. Por tanto dicho proyeccto no debe existir previo a su realización. Se esperan claramente varias referencias de la temática y desarrollo del proyecto que serán revisadas minuciosamente.
* **Evaluación.** Cada fase será tenida en cuenta para dar una nota correspondiente al finalizar todas las fases. ([rubrica]())

### Fases del Proyecto:
1. **FASE 1 - TEMA.** En el archivo ``readme.md`` del repositorio se debe escribir:
	* El Título del Proyecto.
	* Un resumen. Que introduzca brevemente la temática, describa la metodología que se piensa seguir y lo que se espera al finalizar el proyecto. 
	
	*_EVALUACIÓN:_* Un integrante del grupo envía el link del repositorio al grupo de Teams. Se aprobará el tema o se sugerirán cambios hasta ser aprobado.
	
2. **FASE 2 - PRE SUSTENTACIÓN.*** Se espera un repositorio 
	* Donde se entienda cómo esta estructurado el proyecto.
	* Con un nivel razonable de funcionalidad. 
	* Se debe presentar el Objetivo del proyecto: Una frase (este debe aparecer en el ``readme.md``).
	
	*_EVALUACIÓN:_* En clase cada grupo tiene 5 minutos para exponer. En caso de observaciones hechas por el Docente y compañeros, estas se discutirán. Para proseguir con las demas fases, esta discusión debe ser tenida en cuenta.
	
3.  **FASE 3 - ENTREGA DE REVISIÓN**: Entregables.
    - En el ``readme.md`` del repositorio se debe agregar una imagen relacionada (800 x 300) con: Título del proyecto e información de los estudiantes.
    - Compartir al grupo de Teams un video corto (Máximo 5 minutos, formato .mp4 en lo posible)
    - Alojar el video en youtube y agregar el link al ``readme.md``.
    - Archivo PDF de las diapositivas. 

    *_EVALUACIÓN:_* Un integrante del grupo envía por el grupo de Teams el link del repositorio y el archivo del video.
    
    
4.  **FASE 4 - SUSTENTACIÓN.***: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 

	*_EVALUACIÓN:_* En clase cada grupo tiene 5 minutos para exponer. En caso de observaciones hechas por el Docente y compañeros, estas se deben corregir para la entrega final.
	
5. **FASE 5 - ENTREGA FINAL**
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores.
        - Objetivo del proyecto.
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Referencias.
	
	*_EVALUACIÓN:_* Un integrante del grupo envía el link del repositorio ya terminado para ser calificado.

<!--## Proyecto final


Se aceptarán grupos de máximo 3 estudiantes. Realizar el proyecto de forma individual es también aceptado.

Aspectos claves para la evauación del proyecto:

- **01. Funcionamiento del proyecto**: El proyecto se debe realizar como un notebook y debe ser 100% funcional.

- **02. Prototipo (PRE-SUS proyecto)**: Se hará una pre-sustentación del proyecto donde se considere cómo esta estructurado el proyecto y se espera una nivel razonable de funcionalidad. En caso de correciones se deben hacer para proseguir con las demas fases.

- **03. Presentación**: Entregables:
    - Imagen relacionada (800 x 300) con: Título del proyecto e información de los estudiantes.
    - Video corto (Máximo 5 minutos, formato .mp4 en lo posible)
    	* Entregar archivo de video.
    	* Alojar video en youtube.
    - Archivo PDF de las diapositivas.


- **04.Sustentación**: Se realizarán preguntas cortas a los estudiantes unicamente relacionadas con el proyecto. 
 


- **05. Entrega Final (Repositorio del Proyecto)**
    - Todos los archivos relacionados con el proyecto deben alojarse en un repositorio (GitHub o GitLab) de los integrantes del estudainte
    - El archivo readme.md del repositorio debe tener la siguiente información:
        - Titulo del proyecto
        - Banner- Imagen de 800 x 300
        - Autores.
        - Objetivo del proyecto: Una frase
        - Dataset: información con link de descarga
        - Modelos: Métodos usados para su desarrollo. Escribir solo palabras claves. 
        - Enlaces del código, video de la presentación, y repositorio.-->
        
## Calendario estándar
 
                        SESSION 1                SESSION 2

     W01 Nov02-Nov05    Intro MD                 Lógica
     W02 Nov09-Nov12    Lógica                   Demostraciones
     W03 Nov16-Nov19    Demostraciones           Demostraciones
     W04 Nov23-Nov26    Inducción                Inducción
     W05 Nov30-Dic03    Algoritmos               Algoritmos                  
     W06 Dic07-Dic10    Algoritmos               Algoritmos                 
     W07 Dic14-Dic17    Examen                   Laboratorio               PARCIAL NO.1
     
     ----------------   VACACIONES  DIC 17 - ENE 7 --------------

     W08 Ene11-Ene14    Teo. Numeros             Teo. Numeros       
     W09 Ene18-Ene21    Teo. Numeros             Combinatoria           
     W10 Ene25-Ene28    Combinatoria             Combinatoria
     W11 Feb01-Feb04    Laboratorio              Examen                    PARCIAL NO.2
     W12 Feb08-Feb11    Relaciones               Relaciones                F1.Tema
     W13 Feb15-Feb18    Relaciones & Pre-Sus     Relaciones                F2.Pre-Sus
     W14 Feb22-Feb25    Grafos                   Grafos
     W15 Mar01-Mar04    Grafos                   Grafos                    F3.Entrega rev. & F4.Sus
     W16 Mar08-Mar11    Laboratorio	             Examen                    PARCIAL NO.3
         Mar15-Mar18    Entrega	                 -                          F5.Entrega



     Ene 14             -> Registro primera nota
     Ene 16             -> Último día cancelación materias
     Mar 11             -> Finalización clase y
     Mar 14 - Mar 18    ->  evaluaciones finales
     Mar 19             -> Registro calificaciones finales
     Mar 22 - Mar 23    -> habilitaciones
